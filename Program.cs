﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace fr_stringcalculator
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.Title = "Friendlyrentals string calculator kata";
            Console.WriteLine("Follow instructions on 'readme.md'");
            bool end = false;
            while (end == false)
            {
                Console.WriteLine();
                Console.WriteLine("Write a comma separated list of up to 2 numbers to sum, 'END' to finish.");
                string entry = Console.ReadLine();
                if (entry.ToUpper() == "END")
                    end = true;
                else
                {
                    Calculator calculator = new Calculator();
                    int result = calculator.Add(entry);
                    if (result >= 0)
                        Console.WriteLine(string.Format("The sum of the numbers {0} is {1}.", entry, result));
                    Console.WriteLine();
                }
            }
        }
    }
}
