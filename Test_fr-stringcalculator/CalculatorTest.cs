﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using fr_stringcalculator;
using System.Collections.Generic;
using System.Linq;

namespace Test_fr_stringcalculator
{
    [TestClass]
    public class CalculatorTest
    {
        private Calculator calculator;

        public CalculatorTest()
        {
            calculator = new Calculator();
        }

        [TestMethod]
        public void VoidStringReturns0()
        {
            string myInput = string.Empty;
            int expected = 0;
            int result = calculator.Add(myInput);
            Assert.AreEqual(result, expected);
        }

        [TestMethod]
        public void NotNumericStringsRaisesError()
        {
            string myInput = "text";
            int expected = -1;
            int result = calculator.Add(myInput);
            Assert.AreEqual(result, expected);
        }

        [TestMethod]
        public void Sum1NumberIsOk()
        {
            string myInput = "5";
            int expected = 5;
            int result = calculator.Add(myInput);
            Assert.AreEqual(result, expected);
        }

        [TestMethod]
        public void Sum2NumbersIsOk()
        {
            string myInput = "1,2";
            int expected = 3;
            int result = calculator.Add(myInput);
            Assert.AreEqual(result, expected);
        }

        [TestMethod]
        public void SumNNumbersIsOk()
        {
            string myInput = "1,2,3,4,5,6,7,8,9,10";
            int expected = 55;
            int result = calculator.Add(myInput);
            Assert.AreEqual(result, expected);
        }

        [TestMethod]
        public void Sum3NumbersWithCommaAndNewLineIsOk()
        {
            string myInput = "1,\n2,3";
            int expected = 6;
            int result = calculator.Add(myInput);
            Assert.AreEqual(result, expected);
        }

        [TestMethod]
        public void Sum3NumbersWithNewLineAndCommaIsOk()
        {
            string myInput = "1\n,2\n3";
            int expected = 6;
            int result = calculator.Add(myInput);
            Assert.AreEqual(result, expected);
        }
        [TestMethod]
        public void Sum3NumbersWithNewLineIsOk()
        {
            string myInput = "1\n2\n3";
            int expected = 6;
            int result = calculator.Add(myInput);
            Assert.AreEqual(result, expected);
        }

        [TestMethod]
        public void Sum3NumbersWithCustomSeparatorIsOk()
        {
            char separator = ';';
            string myInput = string.Format("//[{0}]\n1;2;3", separator);
            int expected = 6;
            int result = calculator.Add(myInput);
            Assert.AreEqual(result, expected);
        }

        [TestMethod]
        public void Sum3NumbersWithCustomSeparatorSetIsOk()
        {
            List<char> separators = new List<char> { ';', ',' };
            string separatorList = string.Join("", separators.Select(c => string.Format("[{0}]", c)));
            string myInput = string.Format("//{0}\n1;2,3", separatorList);
            int expected = 6;
            int result = calculator.Add(myInput);
            Assert.AreEqual(result, expected);
        }

        [TestMethod]
        public void WrongCustomSeparatorSetRaisesError()
        {
            string myInput = "//[, ]\n1;2;3";
            int expected = -1;
            int result = calculator.Add(myInput);
            Assert.AreEqual(result, expected);
        }

        [TestMethod]
        public void NegativeNumbersRaisesError()
        {
            string myInput = "1,-2,3";
            int expected = -1;
            int result = calculator.Add(myInput);
            Assert.AreEqual(result, expected);
        }
    }
}
