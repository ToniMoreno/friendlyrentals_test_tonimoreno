﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace fr_stringcalculator
{
    public class Calculator
    {
        /// <summary>
        /// Calculates the sum of the numbers in a given string
        /// </summary>
        /// <param name="numbers">String with the numbers to add</param>
        /// <returns>Sum of the numbers in the string, -1 if there's any error</returns>
        public int Add(string numbers)
        {
            // Set a default result
            int result = 0;
            // Set a default separator
            List<char> separators = new List<char> { ',', '\n' };
            // If string starts with "//", we're providing custom separator/s
            if(numbers.StartsWith("//"))
            {
                string[] splittedString = numbers.Substring(2).Split('\n');
                // If there's only 1 splitted string, numbers are not provided
                if (splittedString.Length == 1)
                    numbers = string.Empty;
                else
                {
                    string separatorList = splittedString[0];
                    separators.Clear();
                    foreach (Match splittedSeparator in Regex.Matches(separatorList, @"\[.\]"))
                    {
                        try
                        {
                            char tempSeparator = Convert.ToChar(splittedSeparator.Value.Substring(1, splittedSeparator.Length - 2));
                            separators.Add(tempSeparator);
                        }
                        catch (Exception ex)
                        {
                            Console.WriteLine("Separator list is not valid");
                            return -1;
                        }
                    }
                    numbers = splittedString[1];
                }
            }
            if (!string.IsNullOrEmpty(numbers))
            {
                // Split the given string
                string[] sNumbersArray = numbers.Split(separators.ToArray(), StringSplitOptions.RemoveEmptyEntries);
                int[] iNumbersArray = new int[0];
                try
                {
                    // Convert each string in the array to int
                    iNumbersArray = sNumbersArray.Select(n => int.Parse(n)).ToArray();
                    List<int> negativeNumbers = (iNumbersArray.Where(n => n < 0).ToList());
                    if (negativeNumbers.Count > 0)
                        throw new Exception("Negatives not allowed. You passed " + string.Join(", ", negativeNumbers));
                }
                catch(Exception ex)
                {
                    Console.WriteLine(string.Format("Error found: {0}\n", ex.Message));
                    return -1;
                }
                // Sum of all numbers in the array
                foreach(int n in iNumbersArray)
                {
                    result += n;
                }
            }
            return result;
        }
    }
}
